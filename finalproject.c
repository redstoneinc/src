/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "sirc.h"
#include "servo.h"

//defined variables
	//states
	#define UNLOCK 0
		//is unlocked state
	#define ARMED 1
		//is armed locked state
	#define LOCKED 2
		//is unarmed locked state
	#define ENTERCODE 3
		//is entercode state
		
	//servo positions
	#define LOCKSERVO 1
		//servo in locked pos
	#define UNLOCKSERVO	0
		//servo in unlocked pos
		
	//output pins
	#define SERVOPIN  16
		//output pin of servo
	#define LOCKEDPIN	10
		//output pin for locked state
	#define	ARMEDPIN	11
		//output pin for armed state
	#define UNLOCKEDPIN	12
		//output pin for unlocked state	
	#define	ENTERCODEPIN	13
		//output pin of codeenter state	
	#define PIEZOPIN 9
		//output pin of Piezo
		
	//input pin
	#define VIBRATIONPIN	1
		//input pin of VibrationSensor	
	#define BUTTONPIN1	2
		//input pin of button1
	#define BUTTONPIN2	3
		//input pin of button2
	#define IRPIN	4
		//input pin of IRSensor
	#define HALLPIN	5
		//input pin of HALL Sensor
		
	//constants
	#define ALARMCYCLE	10
		//number of times Alarm sounds
	#define PASSCODELENGTH	4
		//length of passcode

		
		
//functions
	//sensors
		int buttonSense();	//reads inputs from buttons, returns T for correct code, F for incorrect code
		void IR();	//reads IR inputs
		int vibTrigger();	//reads vibration trigger
		int hallSense();	//reads hall sensor
	//actions
		void moveServo(int pos);	//moves servo based on input position(int pos)
		void alarm();	//controls alarm
		void initialize();	//initialize system, locked state
		void setPasscode();	//sets system to accept 4 digit code to replace passcode
		void armed();	//System is armed
		
//global variables
	int *nextState, *state;
	unsigned int stack[40+25];
	int *passcode[4];


/**************TODO**********
	[x]	buttonSense()
	[x]	IR()
	[x]	hallsense()
	[X]	setpasscode()
	[x]	statemachine, potentially main

****************************/

int main()                                    // Main function
{
	initialize();

	int *stMach = cog_run(stateMachine(), 128);
	int *buttons = cog_run(buttonSense(), 128);
}

void moveServo(int pos){	//tested, passed
	switch(pos){
		case LOCKSERVO: //locked
			servo_angle(SERVOPIN, 1800);
			pause(2500);
			printf("Servo Locked\n");
			break;
		case UNLOCKSERVO:
			servo_angle(SERVOPIN,900);
			pause(2500);
			printf("Servo Unlocked\n");
			break;
		default:
			servo_angle(SERVOPIN, 1800);
			pause(2500);
			printf("moveServo Attempted, unknown input\nServo Locked\n");
	}
  servo_stop();
}

void initialize(){
	*nextState = LOCKED;
	moveServo(LOCKSERVO);
	int i;
	for(i=0;i<PASSCODELENGTH;i++)
		*passcode[i] = 1;
}

void alarm(){
		//freqout(piezoPin, duration, frequency);
	int st = ALARMCYCLE;
	while(st){
		freqout(PIEZOPIN, 750, 1300);
		freqout(PIEZOPIN, 750, 500);
		st--;
	}
}

int vibTrigger(){
	if(input(VIBRATIONPIN) == 1)
		return 1;
	else
		return 0;
}

void armed(){//
	while(vibTrigger() == 1 || hallSense() == 1){
		printf("ALARM");
		alarm();
	}
}

void setPasscode(){
	int digit;
	int i = 0;
	while(i<PASSCODELENGTH){
		if(input(0) == 1 && input(1) == 0){
			digit = 1;
			*passcode[i] = digit;
			i++;
		}
		else if (input(0) == 0 && input(1) == 1){
			digit = 2;
			*passcode[i] = digit;
			i++;
		}
		pause(1000);
	}
}

void stateMachine(){
	while(1){
		switch(*state){
			case ARMED:
				high(ARMEDPIN);
				low(LOCKEDPIN);
				low(UNLOCKEDPIN);
				low(ENTERCODEPIN);
				moveServo(LOCKSERVO);
				armed();
				break;
			case LOCKED:
				low(ARMEDPIN);
				high(LOCKEDPIN);
				low(UNLOCKEDPIN);
				low(ENTERCODEPIN);
				moveServo(LOCKSERVO);
				break;
			case UNLOCK:
				low(ARMEDPIN);
				low(LOCKEDPIN);
				high(UNLOCKEDPIN);
				low(ENTERCODEPIN);
				moveServo(UNLOCKSERVO);
				break;
			case ENTERCODE:
				low(ARMEDPIN);
				low(LOCKEDPIN);
				low(UNLOCKEDPIN);
				toggle(ENTERCODEPIN);
				//enter code pin ()
				break;
			default:
				high(ARMEDPIN);
				high(LOCKEDPIN);
				high(UNLOCKEDPIN);
				high(ENTERCODEPIN);
				moveServo(LOCKSERVO);			
		}
		pause(500);
	}
}

int hallSense(){

	
	if(input(HALLPIN)!=1)
		return 1;
	else
		return 0;
}

void buttonSense(){
	int input[4];
	
	int index = 0;
	
	while(index < PASSCODELENGTH){
		if(input(BUTTONPIN1) == 1){
			input[index] = 1;
			index++;
		}
		else if (input(BUTTONPIN2 == 1){
			input[index] = 2;
			index++;
		}
	}
	
	int match = 0;
	for(index == 0;index<PASSCODELENGTH;index++){
		if(*passcode[index] == input[index])
			match = 1;
		else
			match = 0;
	}
	
	if(match)
		*state = UNLOCK;
}

void IR(){
	int inp = sirc_button(IRPIN);
	switch (inp){
		case ENTER: //set passcode
			setPasscode();
			pause(1000);
			break;
		case PWR:	//lock system
			*state = LOCKED
			pause(1000);
			break;
		case CH_UP:	//unlock system
			*state = UNLOCK;
			pause(1000);
			break;
		default:
			printf("Unknown command entered\n");
	}
}
